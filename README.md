##HOW TO RUN
In order to start the whole app it's needed only one command:
``docker compose up`` inside the root folder.

##FLOW AND LOGIC
The flow is pretty basic once an instant is created and if the related photo is bigger than 140x140, a message is
emitted through RabbitMQ and received by the "image-resizer-actor" which will resize, upload the photo and finally
notify the resized photo to the instant-api through a REST endpoint.

##AVAILABLE ENDPOINTS

###CREATE NEW INSTANT
curl --location --request POST 'localhost:3000/instants' \
--header 'x-user: alberto' \
--form 'image=@"{IMAGE_PATH}"' \
--form 'latitude="{LATITUDE}"' \
--form 'longitude="{LONGITUDE}"' \
--form 'timestamp="{TIMESTAMP}"'



###GET ALL INSTANTS
curl --location --request GET 'localhost:3000/instants'
