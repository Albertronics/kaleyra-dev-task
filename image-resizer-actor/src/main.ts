import { AppModule } from './app.module';
import {MicroserviceOptions, Transport} from "@nestjs/microservices";
import {NestFactory} from "@nestjs/core";

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.RMQ,
    options: {
      urls: ['amqp://rabbitmq:5672'],
      queue: 'resize-jobs',
      noAck: false,
      queueOptions: {
        durable: true
      },
    },
  });

  await app.listen();
}
bootstrap();

