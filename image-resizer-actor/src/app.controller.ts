import { Controller } from '@nestjs/common';
import { AppService } from './app.service';
import { Ctx, EventPattern, Payload, RmqContext } from "@nestjs/microservices";
import { ResizeJobData } from "./minio-client/file.model";

@Controller()
export class AppController
{
  constructor(private readonly appService: AppService) {}

  @EventPattern('resize-jobs')
  async getNotifications(@Payload() data: ResizeJobData, @Ctx() context: RmqContext)
  {
    const { resizedImageUrl, weight } = await this.appService.resizeAndSaveImage(data);

    this.appService.notifySuccess({
      url: resizedImageUrl,
      name: `${data.originalname}_resized`,
      weight,
      size: "140x140",
      instant: { id: data.instantId }
    })

    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();

    channel.ack(originalMsg)
  }
}
