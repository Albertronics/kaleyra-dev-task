export interface BufferedFile {
	fieldname: string;
	originalname: string;
	encoding: string;
	mimetype: AppMimeType;
	size: number;
	buffer: Buffer | string;
}

export interface ResizeJobData extends BufferedFile {
	url: string;
	instantId: string;
}

export interface ResizeJobDoneData {
	url: string;
	instant: { id: string };
	weight: number;
	size: string;
	name: string;
}

export interface StoredFile extends HasFile, StoredFileMetadata {}

export interface HasFile {
	file: Buffer | string;
}
export interface StoredFileMetadata {
	id: string;
	name: string;
	encoding: string;
	mimetype: AppMimeType;
	size: number;
	updatedAt: Date;
	fileSrc?: string;
}

export type AppMimeType =
	| 'image/png'
	| 'image/jpeg';
