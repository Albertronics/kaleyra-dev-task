import {Inject, Injectable} from '@nestjs/common';
import {MinioClientService} from "./minio-client/minio-client.service";
import {ResizeJobData, ResizeJobDoneData} from "./minio-client/file.model";
import { request } from 'undici'

const Jimp = require('jimp');

@Injectable()
export class AppService
{
	@Inject(MinioClientService) private readonly minioClientService: MinioClientService

	resizeAndSaveImage(data: ResizeJobData): Promise<{ resizedImageUrl: string, weight: number }>
	{
		return new Promise((resolve, reject) =>
		{
			Jimp.read(data.url, async (err, img) =>
			{
				if(err) {
					return reject(err)
				}

				data.buffer = await img.resize(140, 140).getBufferAsync(data.mimetype);

				const { url } = await this.minioClientService.upload(data);
				resolve({ resizedImageUrl: `http://${url}`, weight: data.buffer.length })
			})
		});
	}

	notifySuccess(payload: ResizeJobDoneData) {
		return request(`http://localhost:3000/instants/${payload.instant.id}/resized-image`, {
			method: "POST",
			body: JSON.stringify(payload),
			headers: {
				"content-type": "application/json"
			}
		})
	}
}
