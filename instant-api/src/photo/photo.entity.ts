import {Entity, Column, PrimaryGeneratedColumn, ManyToOne} from 'typeorm';
import {Instant} from "../instant/instant.entity";

@Entity()
export class Photo
{
	@PrimaryGeneratedColumn("uuid")
	id: string;

	@Column()
	url: string;

	@Column()
	name: string;

	@Column()
	weight: number;

	@Column()
	size: string;

	@Column({ default: false })
	resized: boolean;

	@ManyToOne(() => Instant, instant => instant.photos)
	instant: Instant;
}
