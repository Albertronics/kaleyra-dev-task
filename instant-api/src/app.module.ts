import { Module } from '@nestjs/common';
import { TypeOrmModule } from "@nestjs/typeorm";
import { getConnectionOptions } from "typeorm";
import { InstantModule } from "./instant/instant.module";
import { MinioClientModule } from './minio-client/minio-client.module';
import {MinioModule} from "nestjs-minio-client";
import {config} from "./minio-client/config";

@Module({
	imports: [
		TypeOrmModule.forRootAsync({
			useFactory: async () =>
				Object.assign(await getConnectionOptions(), {
					autoLoadEntities: true,
				}),
		}),
		InstantModule,
		MinioClientModule,
	],
})
export class AppModule {}
