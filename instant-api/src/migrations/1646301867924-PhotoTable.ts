import {MigrationInterface, QueryRunner} from "typeorm";

export class PhotoTable1646301867924 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query(`
             create table photo
            (
                id uuid default uuid_generate_v4() not null
                  constraint "PK_723fa50bf70dcfd06fb5a44d4ff"
                primary key,
                url varchar not null,
                name varchar not null,
                weight integer not null,
                size varchar not null,
                resized boolean default false not null,
                "instantId" uuid
                    constraint "FK_f53ba8a3b6778bc8543faf7240b"
                    references instant
            );      
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
