import {MigrationInterface, QueryRunner} from "typeorm";

export class InstantTable1646301426714 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query(`
            create table instant
            (
                id uuid default uuid_generate_v4() not null
                  constraint "PK_42d498336a383a563137550b239"
                primary key,
                "userName" varchar not null,
                timestamp timestamp with time zone not null,
                latitude integer not null,
                longitude integer not null
            );       
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
