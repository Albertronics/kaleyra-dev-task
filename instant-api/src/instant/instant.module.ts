import { Module } from '@nestjs/common';
import { InstantController } from './instant.controller';
import { InstantService } from './instant.service';
import { TypeOrmModule } from "@nestjs/typeorm";
import { Instant } from "./instant.entity";
import { MinioClientModule } from "../minio-client/minio-client.module";
import {ClientsModule, Transport} from "@nestjs/microservices";
import {Photo} from "../photo/photo.entity";

@Module({
  imports : [
    TypeOrmModule.forFeature([Instant, Photo]),
    MinioClientModule,
    ClientsModule.register([{
      name: 'RESIZE_SERVICE',
      transport: Transport.RMQ,
      options: {
        urls: ['amqp://rabbitmq:5672'],
        queue: 'resize-jobs',
        queueOptions: {
          durable: true
        },
      },
    }]),
  ],
  controllers: [InstantController],
  providers: [InstantService]
})
export class InstantModule {}
