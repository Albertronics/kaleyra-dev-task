import { IsNotEmpty } from 'class-validator';

export class InstantDto
{
	userName: string;

	@IsNotEmpty()
	latitude: number;

	@IsNotEmpty()
	longitude: number;

	@IsNotEmpty()
	timestamp: Date;
}
