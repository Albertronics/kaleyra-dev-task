import {Entity, Column, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import {Photo} from "../photo/photo.entity";
import {InstantDto} from "./instant.dto";

@Entity()
export class Instant
{
	@PrimaryGeneratedColumn("uuid")
	id: string;

	@Column()
	userName: string;

	@OneToMany(() => Photo, photo => photo.instant)
	photos: Photo[];

	@Column({ type: "float" })
	latitude: number;

	@Column({ type: "float" })
	longitude: number;

	@Column({ type: 'timestamptz' })
	timestamp: Date;

	static fromDto(other: InstantDto)
	{
		const newInstant = new Instant();

		newInstant.longitude = other.longitude;
		newInstant.latitude = other.latitude;
		newInstant.timestamp = other.timestamp;
		newInstant.userName = other.userName;

		return newInstant;
	}
}
