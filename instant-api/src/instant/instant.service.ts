import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Instant } from "./instant.entity";
import { InstantDto } from "./instant.dto";
import { MinioClientService } from "../minio-client/minio-client.service";
import { BufferedFile } from "../minio-client/file.model";
import { FilterOperator, PaginateQuery, paginate, Paginated } from 'nestjs-paginate'
import { ClientProxy } from "@nestjs/microservices";
import { Photo } from "../photo/photo.entity";

const probe = require('probe-image-size');

@Injectable()
export class InstantService
{
	@InjectRepository(Instant) private readonly instantRepository: Repository<Instant>
	@InjectRepository(Photo) private readonly photoRepository: Repository<Photo>
	@Inject(MinioClientService) private readonly minioClientService: MinioClientService
	@Inject('RESIZE_SERVICE') private client: ClientProxy

	async getAll(query: PaginateQuery): Promise<Paginated<Instant>> {
		return paginate(query, this.instantRepository, {
			sortableColumns: ['userName', 'timestamp'],
			searchableColumns: ['userName', 'timestamp'],
			defaultSortBy: [['timestamp', 'DESC']],
			filterableColumns: {
				userName: [FilterOperator.GTE, FilterOperator.LTE],
			},
		});
	}

	async create(file: BufferedFile, instant: InstantDto)
	{
		const instantToSave = Instant.fromDto(instant);
		const savedInstant = await this.instantRepository.save(instantToSave);

		const photo = new Photo();

		photo.weight = file.size;
		photo.name = file.originalname;

		const { url } = await this.minioClientService.upload(file);
		photo.url = `http://${url}`;

		const { width, height } = probe.sync(file.buffer);
		photo.size = `${width}x${height}`;
		photo.instant = savedInstant;

		await this.photoRepository.save(photo);

		if(width > 140 || height > 140) {
			this.client.emit('resize-jobs', {
				...file,
				buffer: null,
				url: photo.url,
				instantId: instantToSave.id
			});
		}

		return savedInstant;
	}

	async addResizedPhoto(resizedPhoto: Photo) {
		resizedPhoto.resized = true;
		return this.photoRepository.save(resizedPhoto);
	}

	getInstantResizedUrl(id: string) {
		return this.instantRepository.query(`SELECT url FROM photo WHERE resized = true AND "instantId" = $1 LIMIT 1`, [id])
	}
}
