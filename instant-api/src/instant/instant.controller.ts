import {
	Body,
	Controller,
	Get,
	Post,
	UploadedFile,
	UseInterceptors,
	Headers, BadRequestException, Param, NotFoundException, Res
} from '@nestjs/common';
import { Instant } from "./instant.entity";
import { InstantService } from "./instant.service";
import { FileInterceptor } from "@nestjs/platform-express";
import { InstantDto } from "./instant.dto";
import { BufferedFile } from "../minio-client/file.model";
import { Paginate, Paginated, PaginateQuery } from "nestjs-paginate";
import { Photo } from "../photo/photo.entity";

@Controller('instants')
export class InstantController
{
	constructor(private readonly instantService: InstantService) {}

	@Get()
	findAll(@Paginate() query: PaginateQuery): Promise<Paginated<Instant>> {
		return this.instantService.getAll(query);
	}

	@Get(':id/resized-image')
	async findResizedImage(@Param('id') id: string, @Res() res)
	{
		const [result] = await this.instantService.getInstantResizedUrl(id);

		if(!result) {
			throw new NotFoundException();
		}

		return res.redirect(result.url);
	}

	@Post()
	@UseInterceptors(FileInterceptor('image'))
	create(@UploadedFile() file: BufferedFile, @Body() newInstant: InstantDto, @Headers() headers)
	{
		newInstant.userName = headers["x-user"];

		if(!newInstant.userName) {
			throw new BadRequestException("Username must be specified!");
		}

		return this.instantService.create(file, newInstant);
	}

	@Post(':id/resized-image')
	addResizedImage(@Body() resizedPhoto: Photo) {
		return this.instantService.addResizedPhoto(resizedPhoto);
	}
}
