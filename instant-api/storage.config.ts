import { diskStorage } from "multer";
import { randomUUID } from "crypto";

export const storage = diskStorage({
	destination: "./uploads",
	filename: (req, file, callback) => {
		callback(null, generateFilename(file));
	}
});

function generateFilename(file) {
	const splitFileName = file.originalname.split(".");
	return `${randomUUID()}.${splitFileName[splitFileName.length - 1]}`;
}
